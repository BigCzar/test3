﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrionSaveEditor
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int offsetPlanets = 90855;
        int offsetStars = 96977;
        List<Star> stars;
        byte[] data;
        string path;
        string nameStar = "";

        public MainWindow()
        {
            InitializeComponent();
            path = Properties.Settings.Default.SavePath;
            tbSavePath.Text = path;
            if (path != "") LoadSave();

            if (path != "")
            {

            }
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                path = openFileDialog.FileName;
                tbSavePath.Text = path;
                Properties.Settings.Default.SavePath = path;
                Properties.Settings.Default.Save();
                LoadSave();
            }
        }

        void LoadSave()
        {
            data = File.ReadAllBytes(path);
            tbHex.Text = BitConverter.ToString(data).Replace("-", " ");

            int plnCount = data[offsetPlanets];
            int strCount = data[offsetStars];

            string result = "";
            
            stars = new List<Star>();

            for (int i = 0; i < strCount; i++)
            {
                stars.Add(new Star(i, data.Skip(offsetStars + 2 + i * 113).Take(113).ToArray()));
            }

            for (int i = 0; i < plnCount; i++)
            {
                int offset = offsetPlanets + 2 + i * 17;
                new Planet(stars, offset, ref data);
            }

            stars.ForEach((x => { result += $"{x.Id.ToString("X2")}: {x.Name} - {x.planets.Count}\n"; }));

            tbResult.Text = result;
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Star Sssla = stars.Find(x => x.Name.Contains(nameStar));
            Sssla.planets.ForEach(x =>  x.Set() );
            File.WriteAllBytes(path, data);
            ShowStar();
        }

        private void btnFixOrion_Click(object sender, RoutedEventArgs e)
        {
            Star Orion = stars.Find(x => x.Name.Contains("Orion"));
            Orion.planets.ForEach(x => x.FixOrion());
            File.WriteAllBytes(path, data);
        }

        private void tbResult_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                var tb = (TextBox)sender;

                var charIndex = tb.GetCharacterIndexFromPoint(Mouse.GetPosition(tb), true);
                var lineIndex = tb.GetLineIndexFromCharacterIndex(charIndex);

                var lineText = tb.GetLineText(lineIndex);

                int start = tb.Text.IndexOf(lineText);
                int length = lineText.Length;

                tb.Select(start, length);

                nameStar = (lineText.Split('-')[0].Trim()).Split(':')[1].Trim();

                ShowStar();
            }
        }

        private void ShowStar()
        {
            tbStar.Text = nameStar + "\n\n";
            Star star = stars.Find(x => x.Name.Contains(nameStar));
            star.planets.ForEach(x => tbStar.Text += x.ToString() + "\n");
        }
    }
}


class Planet
{
    byte[] Data;
    int Offset;
    public int StarId;

    public Planet(List<Star> stars, int offset, ref byte[] data)
    {
        Offset = offset;
        Data = data;
        StarId = (int)Data[Offset + 2];

        stars[StarId].planets.Add(this);
    }

    public void Set()
    {
        Data[Offset + 4] = (byte)3;         //Превращает кольца и газовые гиганты в планеты
        Data[Offset + 5] = (byte)4;         //Huge
        Data[Offset + 6] = (byte)1;         //Normal Gravity
        Data[Offset + 8] = (byte)9;         //Gaia    
        Data[Offset + 10] = (byte)4;        //Ultra Rich
        Data[Offset + 11] = (byte)3;        //можно фермеров ставить
        Data[Offset + 15] = (byte)10;       //Ancient Artifacts
    }

    internal void FixOrion()
    {
        if (Data[Offset + 15] == (byte)11)
        {
            Data[Offset + 15] = (byte)10;       //Ancient Artifacts    
        }
    }

    public override string ToString()
    {
        return BitConverter.ToString(Data.Skip(Offset).Take(17).ToArray()).Replace("-", " ");
    }
}

class Star
{
    byte[] Data;
    public int Id;
    public string Name;
    public List<Planet> planets = new List<Planet>();

    public Star(int id, byte[] data)
    {
        Id = id;
        Data = data;
        #region Name
        var dataName = data.Take(15).ToArray();
        int l = Array.IndexOf(dataName, (byte)0);
        Array.Resize(ref dataName, l);
        Name = Encoding.UTF8.GetString(dataName);
        #endregion
    }
}